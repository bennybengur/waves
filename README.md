# Waves

Draws waves on an existing canvas element.

Usage:

`var mywaves = new Waves({divId :"myDiv"});`

`mywaves.addWave(color, lineWidth);` lineWidth in px.

`mywaves.animate();`

Waves Options object:

- divId (mandatory): the div element's id.
- maxWavePower: set the bunderies about and below Zero Y Line (element height /2) in pixels
- speed (default: 0.2)
- numberOfNodes: number of nodes to draw the curves between
- backgroundColor: sets the canvas' background color
- spaceBetweenNodes: along the X axis, in pixels
- dotNumber: 30, // 30 is default
- dotGlobalSpeed: 0.7 // 0.4 is default

TODO: 
- Update Docs. 
- Refactor code (use prototype, etc. feel free to make a PR, btw...).
- Make an NPM package.

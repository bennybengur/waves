function Waves({
    divId,
    maxWavePower: maxWavePower = 30,
    speed: speed = 0.2,
    numberOfNodes: numberOfNodes = 6,
    backgroundColor: backgroundColor = 'white',
    // lineWidth: lineWidth = 2,
    spaceBetweenNodes: spaceBetweenNodes = 274,
    dotNumber: dotNumber = 30,
    dotGlobalSpeed: dotGlobalSpeed = 0.4,
    dotScale: dotScale = 1,
    endsOnThisSide: endsOnThisSide = "right"
}) {
    const div = document.getElementById(divId);

    if (div == null) {
        throw new Error(`Could not find element: ${divId}`);
    }

    div.style.position = "relative";
    const waveCanvas = document.createElement("canvas");
    waveCanvas.style.position = "absolute";
    waveCanvas.style.top = 0;
    waveCanvas.style.left = 0;
    const SCALE_FACTOR = window.devicePixelRatio;
    // cvs.style.borderRadius = "50px 0 0 50px";
    waveCanvas.width = div.offsetWidth * SCALE_FACTOR;
    waveCanvas.height = div.offsetHeight * SCALE_FACTOR;
    div.appendChild(waveCanvas);
    const UP = -1;
    const DOWN = 1;
    const ZERO_Y = Math.round(div.clientHeight * SCALE_FACTOR / 2);
    const MAX_Y = ZERO_Y - maxWavePower * SCALE_FACTOR;
    const MIN_Y = ZERO_Y + maxWavePower * SCALE_FACTOR;

    new Sparkle({
        dotNumber,
        globalSpeed: dotGlobalSpeed,
        scale: dotScale
    });

    const ctx = waveCanvas.getContext("2d");

    var waves = [];

    function getDirectionByY(y) {
        return y > ZERO_Y ? UP : DOWN;
    }

    function randomIntFromInterval(min, max) { // min and max included 
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function Wave(color, lineWidth) {
        var _wave = this;
        //this.maxX = waveCanvas.clientWidth;
        this.speed = speed * SCALE_FACTOR;
        this.color = color;

        this.numNodes = numberOfNodes;

        //instatiate first node
        this.nodes = [{ x: 0, y: randomIntFromInterval(MIN_Y, MAX_Y) }];
        this.nodes[0].wavePower = Math.abs(ZERO_Y - this.nodes[0].y); //wavePower -> max size of node above/under ZERO_Y
        this.nodes[0].currentDirection = getDirectionByY(_wave.nodes[0].y);
        this.nodes[0].speed = this.speed * this.nodes[0].wavePower;

        //instatiates remaining nodes
        this.initWave = function () {
            for (i = 1; i <= _wave.numNodes; i++) {
                // let x = _wave.maxX / _wave.numNodes * i;
                let x = i * spaceBetweenNodes * SCALE_FACTOR;
                let y = randomIntFromInterval(MIN_Y, MAX_Y);
                let currentDirection = _wave.nodes[i - 1].currentDirection * -1;
                let wavePower = Math.abs(y - ZERO_Y);
                let speed = _wave.speed * wavePower;
                _wave.nodes.push({ x, y, speed, wavePower, currentDirection });
            }
            //console.log(_wave.nodes);
        }

        this.initWave();


        this.drawWave = function () {
            ctx.beginPath();
            var pointCount = _wave.nodes.length;
            ctx.moveTo(_wave.nodes[0].x, _wave.nodes[0].y);
            var i;
            for (i = 0; i < pointCount - 1; i++) {
                var c = (_wave.nodes[i].x + _wave.nodes[i + 1].x) / 2;
                var d = (_wave.nodes[i].y + _wave.nodes[i + 1].y) / 2;
                ctx.quadraticCurveTo(_wave.nodes[i].x, _wave.nodes[i].y, c, d);
            }
            ctx.strokeStyle = _wave.color;
            ctx.lineWidth = lineWidth * SCALE_FACTOR;
            ctx.stroke();
        }

        this.update = function () {
            var pointCount = _wave.nodes.length;
            for (i = 0; i < pointCount; i++) {
                _wave.nodes[i].y = _wave.setY(_wave.nodes[i]);
            }
            _wave.drawWave();
        }

        this.setY = function (node) {
            if (node.currentDirection == UP) {
                let y = node.y + (_wave.speed * UP)
                if (y > ZERO_Y - node.wavePower) {
                    return y;
                } else {
                    node.currentDirection *= -1;
                    return ZERO_Y - node.wavePower;
                }
            } else { //DOWN
                let y = node.y + (_wave.speed * DOWN);
                if (y < ZERO_Y + node.wavePower) {
                    return y;
                } else {
                    node.currentDirection *= -1;
                    return ZERO_Y + node.wavePower;
                }
            }
        }
    }

    function Sparkle({
        dotNumber,
        globalSpeed,
        scale
    }) {
        const dotCanvas = document.createElement("canvas");
        const ctx = dotCanvas.getContext('2d');
        const edges = {
            left: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAABkCAYAAAD0ZHJ6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFWSURBVGhD7ZlBEsIgEASj//+zZqrkoIICsywLmb7ose2BQ+LtcXKQ3E5eX825vz4pLH5kCRPBkZgJjqpoWnCEpPnE1pJDzqCl5HUuySdWFYcWtJC87sQJtqJLQUZSEyd6K7oW7JHUxJ+0VlTBHC0VVZBFgiVqz6EKskiQRYIlal84qWCOltd17oItcsBVsFUOuAn2yAEXwV45MFyQkQMmL9FzsGIJc0ErsYTpxNZyIPz/JJTgSLFE98QecqC5oJdYolrQWyxRNfEsOfBXcKYc+Ck4Ww4UBSPIgaxgFDnwJRhJDrwJRpMDPy9JBNYRjDgv0MQsEmSRIIsEWSTIso5g6+OnF5qYZS3BiOdQE7N8CUabec2JI1XUJWEpCkaZee2JI1Rc/5LMrrh+wdlUCc6ceZ+JZ1Xc65LMqLhXQeBdcb+CwLPingWBV8V9CwKPinsXBGMrHscTSVCQkv56tqAAAAAASUVORK5CYII=",
            right: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAABkCAYAAAD0ZHJ6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFVSURBVGhD7dfbbsIwEEXRtP//z7RbwhKKcrE9F4/CWS/wuHWGtPCzBXr9e7+dFhboEYff92tZIQt6rQf3QM84uAZ6x8EtMCIO3/GQRK0Hc2BkHJ594uj1MB2YEYdnnjhrPQwHZsbhWSfOXg/PWXDFeii/oAKtuj6Dqz5/0ImtFGj1jKcY+k9yovy3maFAZEcOByIzcioQWZHTgciINAUiOtIc2ESFugU23qHugfCMDAlsPEJDAxtLaEogZiPTApvR0PTApjd0WSB6IpcG4i5yeSCuIksE4iyyTCCOIksFYh9ZLhCfkfrhblXyxGhn1omtFGilQCsFWinQqnygvm5ZKdCq/G8Sndiq1In354UW7HW0HvSQWJU48dl5oQXvXK0HLXjlbj3oz4zVshP3nBda8EjvetCCeyPrQQt+Gl0PWrCZWQ9aELPrQQta1sN3L2hdb9u27Q9VINPAek8bqwAAAABJRU5ErkJggg=="
        }

        let dot = new Image;
        dot.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAHCAYAAAA1WQxeAAAABHNCSVQICAgIfAhkiAAAAItJREFUCFtljysOwlAQRc+VBAQYLG8NrKCSVuFr0GyAnWARJGgESStZyqvCQQUhJCQd0kwhhI6858xP/FdIxsRz/Yn15SFdYuyRRpgdqIq8ZS6ERYLphDT0huYFz2k7yYVZ+kAaODQw3VAzJ5axE7IrsknH72AbqnL7syJbATvMLog1sTj2j+y948EbBzgpCPDMsTMAAAAASUVORK5CYII=";
        let edge = document.createElement("img");
        var att = document.createAttribute("aria-hidden");
        att.value = "true";
        edge.setAttributeNode(att);
        edge.src = edges[endsOnThisSide];
        edge.style.position = "absolute";
        edge.style.height = div.offsetHeight + "px";


        dot.addEventListener('load', function () {
            dotCanvas.style.position = "absolute";
            dotCanvas.style.top = 0;
            dotCanvas.style.left = 0;
            div.appendChild(dotCanvas);
            div.appendChild(edge)
            var w, h;
            function resize() {
                w = dotCanvas.width = waveCanvas.width = div.offsetWidth * SCALE_FACTOR; h = dotCanvas.height = waveCanvas.height = div.offsetHeight * SCALE_FACTOR;
                edge.style.height = div.offsetHeight + "px";
                if (endsOnThisSide == "left") {
                    edge.style.left - "0px";
                } else {
                    edge.style.left = `${div.offsetWidth - edge.width}px`;
                }
            }
            dotCanvas.style.transformOrigin = waveCanvas.style.transformOrigin = "top left";
            dotCanvas.style.transform = waveCanvas.style.transform = `scale(${1 / SCALE_FACTOR})`;
            resize();
            window.addEventListener("resize", resize);
            function rand(min, max) { return Math.random() * (max ? (max - min) : min) + (max ? min : 0) }

            function DO(count, callback) { while (count--) { callback(count) } }

            const sprites = [];
            DO(dotNumber, () => {
                sprites.push({
                    x: rand(w), y: rand(h),
                    xr: 0, yr: 0, // actual position of sprite
                    r: rand(Math.PI * 2), // rotation
                    scale: rand(0.5, 1) * SCALE_FACTOR,
                    dx: rand(-0.5, 0.5) * SCALE_FACTOR, dy: rand(-0.5, 0.5) * SCALE_FACTOR, //dot speed
                    dr: rand(-0.2, 0.2),
                    countD: 0, // distance travelled in current direction
                    reverse: 1, // currently back or forth
                    d: rand(100, 150) * SCALE_FACTOR // distance to travel in each direction
                });
            });

            function getOffset(el) {
                var _x = 0;
                var _y = 0;
                while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
                    _x += el.offsetLeft - el.scrollLeft;
                    _y += el.offsetTop - el.scrollTop;
                    el = el.offsetParent;
                }
                return { top: _y, left: _x };
            }

            function drawImage(image, spr) {
                ctx.setTransform(spr.scale * scale, 0, 0, spr.scale * scale, spr.xr, spr.yr); // sets scales and origin
                ctx.rotate(spr.r);
                ctx.drawImage(image, -image.width / 2, -image.height / 2);
            }

            function setDirection(spr) {
                if (spr.countD >= spr.d || spr.countD <= -(spr.d)) {
                    spr.reverse *= -1;
                }
                spr.x += spr.dx * (1 - Math.abs(spr.countD / spr.d)) * spr.reverse * globalSpeed;
                spr.y += spr.dy * (1 - Math.abs(spr.countD / spr.d)) * spr.reverse * globalSpeed;
                spr.countD += spr.reverse;
            }
            function update() {
                var ihM, iwM;
                ctx.setTransform(1, 0, 0, 1, 0, 0);
                ctx.clearRect(0, 0, w, h);
                if (dot.complete) {
                    var iw = dot.width;
                    var ih = dot.height;
                    for (var i = 0; i < sprites.length; i++) {
                        var spr = sprites[i];
                        setDirection(spr);
                        spr.r += spr.dr;
                        iwM = iw * spr.scale * 2 + w;
                        ihM = ih * spr.scale * 2 + h;
                        spr.xr = ((spr.x % iwM) + iwM) % iwM - iw * spr.scale;
                        spr.yr = ((spr.y % ihM) + ihM) % ihM - ih * spr.scale;
                        drawImage(dot, spr);
                    }
                }
                requestAnimationFrame(update);
            }
            requestAnimationFrame(update);

        }, false);

    }

    function update() {
        ctx.fillStyle = backgroundColor;
        ctx.globalCompositeOperation = "source-over";
        ctx.fillRect(0, 0, waveCanvas.width, waveCanvas.height);

        waves.forEach(w => {
            w.update();
        });

        requestAnimationFrame(update);
    }

    return {
        addWave: function (color = "#005792", lineWidth = 2) {
            waves.push(new Wave(color, lineWidth));
        },
        animate: function () {
            update();
        }
    }
}